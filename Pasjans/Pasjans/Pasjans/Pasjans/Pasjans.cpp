#include "Pasjans.h"

CKarta CTalia::InicjalizacjaTalii(CKarta Karta[], char nazwa)
{
	for (int i = 1; i < KARTY; i++)
	{
		Karta[i]._Wartosc = i;
		Karta[i]._Symbol = nazwa;
	}
	return CKarta();
}

CKarta CTalia::LaczenieWTalie(CKarta Trefl[], CKarta Pik[], CKarta Karo[], CKarta Serce[])
{
	int j = 1;

	for (int i = 1; i < 14; i++)
	{
		_Talia[i]._Wartosc = Trefl[i]._Wartosc;
		_Talia[i]._Symbol = Trefl[i]._Symbol;
	}
	j = 1;
	for (int i = 14; i < 27; i++)
	{
		_Talia[i]._Wartosc = Pik[j]._Wartosc;
		_Talia[i]._Symbol = Pik[j]._Symbol;
		j++;
	}
	j = 1;
	for (int i = 27; i < 40; i++)
	{
		_Talia[i]._Wartosc = Karo[j]._Wartosc;
		_Talia[i]._Symbol = Karo[j]._Symbol;
		j++;
	}
	j = 1;
	for (int i = 40; i < 53; i++)
	{
		_Talia[i]._Wartosc = Serce[j]._Wartosc;
		_Talia[i]._Symbol = Serce[j]._Symbol;
		j++;
	}
	return CKarta();
}

CKarta CTalia::LosowanieTalii()
{
	random_shuffle(&_Talia[1], &_Talia[TALIA]);
	return CKarta();
}

CKarta CTalia::Zerowanie_array()
{
	for (int i = 0; i < KARTY; i++)
	{
		Stosik_A[i]._Wartosc = 0;
		Stosik_B[i]._Wartosc = 0;
		Stosik_C[i]._Wartosc = 0;
		Stosik_D[i]._Wartosc = 0;
		Stosik_E[i]._Wartosc = 0;
		Stosik_F[i]._Wartosc = 0;
		Stosik_G[i]._Wartosc = 0;
		Stosik_H[i]._Wartosc = 0;
		Stosik_I[i]._Wartosc = 0;
		Kolejka[i]._Wartosc = 0;
	}
	return CKarta();
}

int CTalia::Zliczanie_elementow_array(array<CKarta, KARTY> talia)
{
	int i;
	for (i = 1; i < KARTY; i++)
	{
		if (talia[i]._Wartosc == 0) return i - 1;
		if (i == KARTY - 1) return i;
	}
}

int CTalia::Zliczanie_elementow_array2(array<CKarta, 35> talia)
{
	int i;
	for (i = 1; i < 35; i++)
	{
		if (talia[i]._Wartosc == 0) return i - 1;
		if (i == 34) return i;
	}
}

CKarta CTalia::Destrukcja_Niepotrzebnych_Tablic(CKarta Pomniejsze_Talie[])
{
	delete[] Pomniejsze_Talie;
	return CKarta();
}

CKarta CTalia::Podzial_na_stosiki()
{
	//Na stos A
	for (int i = 1; i < KARTY; i++)
	{
		Stosik_A[i] = _Talia[i];
	}
	//Na stos B
	Stosik_B[1] = _Talia[KARTY];

	//Na Kolejka
	int l = 1;
	for (int i = 19; i < TALIA; i++)
	{
		Kolejka[l] = _Talia[i];
		l++;
	}
	return CKarta();
}

CKarta CTalia::Wypisanie_stosu_A(array <CKarta, KARTY> stosik)
{
	int ilosc = Zliczanie_elementow_array(stosik);
	cout << "1. Do dobrania: " << setw(2) << ilosc << " kart" << endl;
	int a = Zliczanie_elementow_array(stosik);
	cout << "Mozna dobrac: " << endl << setw(5) << stosik[a]._Wartosc << "-" << stosik[a]._Symbol;
	return CKarta();
}

CKarta CTalia::Stol_B(int x, int y, int nazwa_stosu)
{
	int a = Zliczanie_elementow_array(Stosik_B);
	COORD c;
	c.X = x;
	c.Y = y;

	COORD d;
	d.X = x;
	d.Y = y - 1;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), d);
	cout << nazwa_stosu << ".";
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << Stosik_B[a]._Wartosc << "-" << Stosik_B[a]._Symbol;
	return CKarta();
}

CKarta CTalia::Stol(array <CKarta, KARTY> Stos, int x, int y, int nazwa)
{
	COORD c;
	c.X = x;
	c.Y = y;

	COORD d;
	d.X = x;
	d.Y = y - 1;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), d);
	cout << nazwa << ".";
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << Stos[0]._Wartosc;
	return CKarta();
}

CKarta CTalia::Stol_3(array <CKarta, 35> Stos, int x, int y, int nazwa)
{
	COORD c;
	c.X = x;
	c.Y = y;

	COORD d;
	d.X = x;
	d.Y = y - 1;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), d);
	cout << nazwa << ". Do dobrania zostalo: " << Kolejka.size() << " kart";
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	
	cout << "Mozna dobrac: " << Kolejka.back()._Wartosc << "-" << Kolejka.back()._Symbol;
	return CKarta();
}

CKarta CTalia::Stol_2(array<CKarta, KARTY> Stos, int x, int y, int nazwa, int numer_z_Talia)
{
	Stos[1] = _Talia[numer_z_Talia];

	COORD c;
	c.X = x;
	c.Y = y;

	COORD d;
	d.X = x;
	d.Y = y - 1;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), d);
	cout << nazwa << ".";
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << Stos[1]._Wartosc << "-" << Stos[1]._Symbol;
	return CKarta();
}

void CTalia::Rysuj_podzial(int x, int y)
{
	COORD c;
	c.X = x;
	c.Y = y;

	COORD d;
	d.X = x;
	d.Y = y - 1;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), d);
	cout << "|";
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "|";
}

CKarta CTalia::Ruch(int skad, int dokad)
{
	// ZAKONCZENIE GRRRRY
	if (skad == 0 && dokad == 0)
	{
		cout << "\n---------------- GRA ZOSTALA ZAKONCZONA ----------------\n";
		Sleep(1000);
		exit(0);
	}

	// RUCHY NIEDOZWOLONE
	if ((skad == dokad) || (dokad == 1) || (dokad == 10) || (skad == 2 && skad == 3) || (skad == 2 && skad == 4) ||
		(skad == 2 && skad == 5) || (skad == 3 && skad == 2) || (skad == 3 && skad == 4) || (skad == 3 && skad == 5) ||
		(skad == 4 && skad == 2) || (skad == 4 && skad == 3) || (skad == 4 && skad == 5) || (skad == 5 && skad == 2) ||
		(skad == 5 && skad == 3) || (skad == 5 && skad == 4)) cout << "Operacja niedozwolona";

	// RUCHY DOZWOLONE
	if (skad == 1 && dokad == 2)
	{
		int a = Zliczanie_elementow_array(Stosik_A);
		int b = Zliczanie_elementow_array(Stosik_B);
		if ((Stosik_A[a]._Wartosc == Stosik_B[b]._Wartosc + 1) && (Stosik_A[a]._Symbol == Stosik_B[b]._Symbol))
		{
			Stosik_B[b+1] = Stosik_A[a];
			Stosik_A[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 1 && dokad == 3)
	{
		int a = Zliczanie_elementow_array(Stosik_A);
		int b = Zliczanie_elementow_array(Stosik_C);
		if (Stosik_A[a]._Wartosc == Stosik_B[b]._Wartosc)
		{
			Stosik_C[b + 1] = Stosik_A[a];
			Stosik_A[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 1 && dokad == 4)
	{
		int a = Zliczanie_elementow_array(Stosik_A);
		int b = Zliczanie_elementow_array(Stosik_D);
		if (Stosik_A[a]._Wartosc == Stosik_B[b]._Wartosc)
		{
			Stosik_D[b + 1] = Stosik_A[a];
			Stosik_A[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 1 && dokad == 5)
	{
		int a = Zliczanie_elementow_array(Stosik_A);
		int b = Zliczanie_elementow_array(Stosik_E);
		if (Stosik_A[a]._Wartosc == Stosik_B[b]._Wartosc)
		{
			Stosik_E[b + 1] = Stosik_A[a];
			Stosik_A[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 1 && dokad == 6)
	{
		int a = Zliczanie_elementow_array(Stosik_A);
		int b = Zliczanie_elementow_array(Stosik_F);
		if (Stosik_A[a]._Wartosc == Stosik_F[b]._Wartosc + 1)
		{
			Stosik_F[b + 1] = Stosik_A[a];
			Stosik_A[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 1 && dokad == 7)
	{
		int a = Zliczanie_elementow_array(Stosik_A);
		int b = Zliczanie_elementow_array(Stosik_G);
		if (Stosik_A[a]._Wartosc == Stosik_G[b]._Wartosc + 1)
		{
			Stosik_G[b + 1] = Stosik_A[a];
			Stosik_A[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 1 && dokad == 8)
	{
		int a = Zliczanie_elementow_array(Stosik_A);
		int b = Zliczanie_elementow_array(Stosik_F);
		if (Stosik_A[a]._Wartosc == Stosik_H[b]._Wartosc + 1)
		{
			Stosik_H[b + 1] = Stosik_A[a];
			Stosik_A[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 1 && dokad == 9)
	{
		int a = Zliczanie_elementow_array(Stosik_A);
		int b = Zliczanie_elementow_array(Stosik_I);
		if (Stosik_A[a]._Wartosc == Stosik_I[b]._Wartosc + 1)
		{
			Stosik_I[b + 1] = Stosik_A[a];
			Stosik_A[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}

	if (skad == 6 && dokad == 2)
	{
		int a = Zliczanie_elementow_array(Stosik_F);
		int b = Zliczanie_elementow_array(Stosik_B);
		if (Stosik_F[a]._Wartosc == Stosik_B[b]._Wartosc + 1)
		{
			Stosik_B[b + 1] = Stosik_F[a];
			Stosik_F[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 6 && dokad == 3)
	{
		int a = Zliczanie_elementow_array(Stosik_F);
		int b = Zliczanie_elementow_array(Stosik_C);
		if (Stosik_F[a]._Wartosc == Stosik_C[b]._Wartosc + 1)
		{
			Stosik_C[b + 1] = Stosik_F[a];
			Stosik_F[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 6 && dokad == 4)
	{
		int a = Zliczanie_elementow_array(Stosik_F);
		int b = Zliczanie_elementow_array(Stosik_D);
		if (Stosik_F[a]._Wartosc == Stosik_D[b]._Wartosc + 1)
		{
			Stosik_D[b + 1] = Stosik_F[a];
			Stosik_F[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 6 && dokad == 5)
	{
		int a = Zliczanie_elementow_array(Stosik_F);
		int b = Zliczanie_elementow_array(Stosik_E);
		if (Stosik_F[a]._Wartosc == Stosik_E[b]._Wartosc + 1)
		{
			Stosik_E[b + 1] = Stosik_F[a];
			Stosik_F[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 6 && dokad == 7)
	{
		int a = Zliczanie_elementow_array(Stosik_F);
		int b = Zliczanie_elementow_array(Stosik_G);
		if (Stosik_F[a]._Wartosc == Stosik_G[b]._Wartosc + 1)
		{
			Stosik_G[b + 1] = Stosik_F[a];
			Stosik_F[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 6 && dokad == 8)
	{
		int a = Zliczanie_elementow_array(Stosik_F);
		int b = Zliczanie_elementow_array(Stosik_H);
		if (Stosik_F[a]._Wartosc == Stosik_H[b]._Wartosc + 1)
		{
			Stosik_H[b + 1] = Stosik_F[a];
			Stosik_F[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 6 && dokad == 9)
	{
		int a = Zliczanie_elementow_array(Stosik_F);
		int b = Zliczanie_elementow_array(Stosik_I);
		if (Stosik_F[a]._Wartosc == Stosik_I[b]._Wartosc + 1)
		{
			Stosik_I[b + 1] = Stosik_F[a];
			Stosik_F[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}

	if (skad == 7 && dokad == 2)
	{
		int a = Zliczanie_elementow_array(Stosik_G);
		int b = Zliczanie_elementow_array(Stosik_B);
		if (Stosik_G[a]._Wartosc == Stosik_B[b]._Wartosc + 1)
		{
			Stosik_B[b + 1] = Stosik_G[a];
			Stosik_G[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 7 && dokad == 3)
	{
		int a = Zliczanie_elementow_array(Stosik_G);
		int b = Zliczanie_elementow_array(Stosik_C);
		if (Stosik_G[a]._Wartosc == Stosik_C[b]._Wartosc + 1)
		{
			Stosik_C[b + 1] = Stosik_G[a];
			Stosik_G[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 7 && dokad == 4)
	{
		int a = Zliczanie_elementow_array(Stosik_G);
		int b = Zliczanie_elementow_array(Stosik_D);
		if (Stosik_G[a]._Wartosc == Stosik_D[b]._Wartosc + 1)
		{
			Stosik_D[b + 1] = Stosik_G[a];
			Stosik_G[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 7 && dokad == 5)
	{
		int a = Zliczanie_elementow_array(Stosik_G);
		int b = Zliczanie_elementow_array(Stosik_E);
		if (Stosik_G[a]._Wartosc == Stosik_E[b]._Wartosc + 1)
		{
			Stosik_E[b + 1] = Stosik_G[a];
			Stosik_G[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 7 && dokad == 6)
	{
		int a = Zliczanie_elementow_array(Stosik_G);
		int b = Zliczanie_elementow_array(Stosik_F);
		if (Stosik_G[a]._Wartosc == Stosik_F[b]._Wartosc + 1)
		{
			Stosik_F[b + 1] = Stosik_G[a];
			Stosik_G[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 7 && dokad == 8)
	{
		int a = Zliczanie_elementow_array(Stosik_G);
		int b = Zliczanie_elementow_array(Stosik_H);
		if (Stosik_G[a]._Wartosc == Stosik_H[b]._Wartosc + 1)
		{
			Stosik_H[b + 1] = Stosik_G[a];
			Stosik_G[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 7 && dokad == 9)
	{
		int a = Zliczanie_elementow_array(Stosik_G);
		int b = Zliczanie_elementow_array(Stosik_I);
		if (Stosik_G[a]._Wartosc == Stosik_I[b]._Wartosc + 1)
		{
			Stosik_I[b + 1] = Stosik_G[a];
			Stosik_G[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}

	if (skad == 8 && dokad == 2)
	{
		int a = Zliczanie_elementow_array(Stosik_H);
		int b = Zliczanie_elementow_array(Stosik_B);
		if (Stosik_H[a]._Wartosc == Stosik_B[b]._Wartosc + 1)
		{
			Stosik_B[b + 1] = Stosik_H[a];
			Stosik_H[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 8 && dokad == 3)
	{
		int a = Zliczanie_elementow_array(Stosik_H);
		int b = Zliczanie_elementow_array(Stosik_C);
		if (Stosik_H[a]._Wartosc == Stosik_C[b]._Wartosc + 1)
		{
			Stosik_C[b + 1] = Stosik_H[a];
			Stosik_H[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 8 && dokad == 4)
	{
		int a = Zliczanie_elementow_array(Stosik_H);
		int b = Zliczanie_elementow_array(Stosik_D);
		if (Stosik_H[a]._Wartosc == Stosik_D[b]._Wartosc + 1)
		{
			Stosik_D[b + 1] = Stosik_H[a];
			Stosik_H[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 8 && dokad == 5)
	{
		int a = Zliczanie_elementow_array(Stosik_H);
		int b = Zliczanie_elementow_array(Stosik_E);
		if (Stosik_H[a]._Wartosc == Stosik_E[b]._Wartosc + 1)
		{
			Stosik_E[b + 1] = Stosik_H[a];
			Stosik_H[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 8 && dokad == 6)
	{
		int a = Zliczanie_elementow_array(Stosik_H);
		int b = Zliczanie_elementow_array(Stosik_F);
		if (Stosik_H[a]._Wartosc == Stosik_F[b]._Wartosc + 1)
		{
			Stosik_F[b + 1] = Stosik_H[a];
			Stosik_H[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 8 && dokad == 7)
	{
		int a = Zliczanie_elementow_array(Stosik_H);
		int b = Zliczanie_elementow_array(Stosik_G);
		if (Stosik_H[a]._Wartosc == Stosik_G[b]._Wartosc + 1)
		{
			Stosik_G[b + 1] = Stosik_H[a];
			Stosik_H[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 8 && dokad == 9)
	{
		int a = Zliczanie_elementow_array(Stosik_H);
		int b = Zliczanie_elementow_array(Stosik_I);
		if (Stosik_H[a]._Wartosc == Stosik_I[b]._Wartosc + 1)
		{
			Stosik_I[b + 1] = Stosik_H[a];
			Stosik_H[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	
	if (skad == 9 && dokad == 2)
	{
		int a = Zliczanie_elementow_array(Stosik_I);
		int b = Zliczanie_elementow_array(Stosik_B);
		if (Stosik_I[a]._Wartosc == Stosik_B[b]._Wartosc + 1)
		{
			Stosik_B[b + 1] = Stosik_I[a];
			Stosik_I[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}

	}
	if (skad == 9 && dokad == 3)
	{
		int a = Zliczanie_elementow_array(Stosik_I);
		int b = Zliczanie_elementow_array(Stosik_C);
		if (Stosik_I[a]._Wartosc == Stosik_C[b]._Wartosc + 1)
		{
			Stosik_C[b + 1] = Stosik_I[a];
			Stosik_I[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 9 && dokad == 4)
	{
		int a = Zliczanie_elementow_array(Stosik_I);
		int b = Zliczanie_elementow_array(Stosik_D);
		if (Stosik_I[a]._Wartosc == Stosik_D[b]._Wartosc + 1)
		{
			Stosik_D[b + 1] = Stosik_I[a];
			Stosik_I[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 9 && dokad == 5)
	{
		int a = Zliczanie_elementow_array(Stosik_I);
		int b = Zliczanie_elementow_array(Stosik_E);
		if (Stosik_I[a]._Wartosc == Stosik_E[b]._Wartosc + 1)
		{
			Stosik_E[b + 1] = Stosik_I[a];
			Stosik_I[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 9 && dokad == 6)
	{
		int a = Zliczanie_elementow_array(Stosik_I);
		int b = Zliczanie_elementow_array(Stosik_F);
		if (Stosik_I[a]._Wartosc == Stosik_F[b]._Wartosc + 1)
		{
			Stosik_F[b + 1] = Stosik_I[a];
			Stosik_I[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 9 && dokad == 7)
	{
		int a = Zliczanie_elementow_array(Stosik_I);
		int b = Zliczanie_elementow_array(Stosik_G);
		if (Stosik_I[a]._Wartosc == Stosik_G[b]._Wartosc + 1)
		{
			Stosik_G[b + 1] = Stosik_I[a];
			Stosik_I[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	if (skad == 9 && dokad == 8)
	{
		int a = Zliczanie_elementow_array(Stosik_I);
		int b = Zliczanie_elementow_array(Stosik_H);
		if (Stosik_I[a]._Wartosc == Stosik_H[b]._Wartosc + 1)
		{
			Stosik_H[b + 1] = Stosik_I[a];
			Stosik_I[a]._Wartosc = 0;
		}
		else
		{
			cout << "\nRuch niedozwolony!\n";
		}
	}
	
	return CKarta();
}

CTalia::CTalia()
{
}

CTalia::CTalia(int ilosc_losowan)
{
	InicjalizacjaTalii(_Trefl, '\x5');
	InicjalizacjaTalii(_CzarneSerce, '\x6');
	InicjalizacjaTalii(_Karo, '\x4');
	InicjalizacjaTalii(_CzerwoneSerca, '\x3');
	LaczenieWTalie(_Trefl, _CzarneSerce, _Karo, _CzerwoneSerca);
	Zerowanie_array();
	Destrukcja_Niepotrzebnych_Tablic(_Trefl);
	Destrukcja_Niepotrzebnych_Tablic(_CzarneSerce);
	Destrukcja_Niepotrzebnych_Tablic(_Karo);
	Destrukcja_Niepotrzebnych_Tablic(_CzerwoneSerca);

	for (int i = 0; i < ilosc_losowan; i++)
	{
		LosowanieTalii();
	}

	Podzial_na_stosiki();
	Wypisanie_stosu_A(Stosik_A);;
	Stol_B(5, 5);
	Stol(Stosik_C, 12, 5, 3);
	Stol(Stosik_D, 19, 5, 4);
	Stol(Stosik_E, 26, 5, 5);
	Rysuj_podzial(30, 5);
	Stol_2(Stosik_F, 34, 5, 6, 15);
	Stol_2(Stosik_G, 42, 5, 7, 16);
	Stol_2(Stosik_H, 49, 5, 8, 17);
	Stol_2(Stosik_I, 56, 5, 9, 18);
	Stol_3(Kolejka, 0, 22, 10);
	Destrukcja_Niepotrzebnych_Tablic(_Talia);
}

CTalia::~CTalia()
{
}