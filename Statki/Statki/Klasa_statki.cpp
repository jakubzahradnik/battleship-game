#include "Klasa_statki.h"
#include "Funkcje.h"

using namespace std;

// FRIENDSY FUNKCJE
void f_ryruj_plansze(CPlansza x[])
{
	for (int i = 0; i < 100; i++)
	{
		// KURSOR
		if (x[i]._Kursor_do_poruszania == true)
		{
			kolor_tekstu(14);
			cout << " ";
			cout << "[X]";
			kolor_tekstu(15);
		}
		// STATKI symbol + kolor
		else if ((x[i]._Wlasciciel_statku == 2) && (x[i]._Strzal == false))
		{
			kolor_tekstu(9);
			cout << " ";
			cout << "[#]";
			kolor_tekstu(15);
		}
		// STATKI po trafieniu
		else if ((x[i]._Strzal == true) && (x[i]._Trafienie == true))
		{
			kolor_tekstu(10);
			cout << " ";
			cout << "[#]";
			kolor_tekstu(15);
		}
		// POLA nie trafione
		else if ((x[i]._Strzal == true) && (x[i]._Pudlo == true))
		{
			kolor_tekstu(12);
			cout << " ";
			cout << "[#]";
			kolor_tekstu(15);
		}
		// POLA planszy
		else if (x[i]._Strzal == false)
		{
			cout << " ";
			cout << "[" << (char)177 << "]";
		}

		if ((i + 1) % 10 == 0) cout << endl;
	}
}

bool f_strzal_gracza(CPlansza *x, int &_ID, int &c, int &l, int &t, int y, bool &strzal_wykonany)
{
	if ((y < 100) && (y >= 0))
	{
		if (x[y]._Strzal == true)
		{
			kolor_tekstu(14);
			cout << endl << "[GRACZ] Juz tu strzelano!" << endl;
			kolor_tekstu(15);
			strzal_wykonany = false;
		}
		else
		{
			if (x[y]._Ukryty == true)
			{
				kolor_tekstu(10);
				cout << endl << "[GRACZ] Trafiono wrogi statek!" << endl;
				kolor_tekstu(15);

				x[y]._Strzal = true;
				x[y]._Trafienie = true;
				_ID = x[y]._ID_statku;
				c = x[y]._Wspolrzedna;
				l--;
				t++;
				strzal_wykonany = false;
				Sleep(1000);
				return true;
			}
			else
			{
				kolor_tekstu(12);
				cout << endl << "[GRACZ] Pudlo! " << endl;
				x[y]._Strzal = true;
				x[y]._Pudlo = true;
				strzal_wykonany = true;
				t++;
				kolor_tekstu(15);
			}
		}
	}
	else
	{
		kolor_tekstu(14);
		cout << endl << "[GRACZ] BLAD! Pole nie istnieje!" << endl;
		kolor_tekstu(15);
		strzal_wykonany = false;
	}

	Sleep(1000);
	return false;
}

bool f_strzal_AI(CPlansza *x, int &_ID, int &c, int &l, int &ostatni_strzal, bool &strzal_wykonany)
{
	int r;
	bool losowanie = false;

	do
	{
		r = rand() % 100;

		if (ostatni_strzal > 0)
		{
			if ((x[ostatni_strzal - 1]._Strzal == false) && (ostatni_strzal > 0) && (ostatni_strzal % 10 != 0))
			{
				r = ostatni_strzal - 1;
			}
			else if ((x[ostatni_strzal - 1]._Trafienie == true) && (x[ostatni_strzal - 2]._Strzal == false) && (ostatni_strzal - 1 > 0) && ((ostatni_strzal - 1) % 10 != 0))
			{
				r = ostatni_strzal - 2;
			}
			else if ((x[ostatni_strzal - 2]._Trafienie == true) && (x[ostatni_strzal - 3]._Strzal == false) && (ostatni_strzal - 2 > 0) && ((ostatni_strzal - 2) % 10 != 0))
			{
				r = ostatni_strzal - 3;
			}
			else if ((x[ostatni_strzal + 1]._Strzal == false) && (ostatni_strzal + 1 < 100) && ((ostatni_strzal + 1) % 10 != 0))
			{
				r = ostatni_strzal + 1;
			}
			else if ((x[ostatni_strzal + 1]._Trafienie == true) && (x[ostatni_strzal + 2]._Strzal == false) && (ostatni_strzal + 2 < 100) && ((ostatni_strzal + 2) % 10 != 0))
			{
				r = ostatni_strzal + 2;
			}
			else if ((x[ostatni_strzal + 2]._Trafienie == true) && (x[ostatni_strzal + 3]._Strzal == false) && (ostatni_strzal + 3 < 100) && ((ostatni_strzal + 3) % 10 != 0))
			{
				r = ostatni_strzal + 3;
			}
			else if ((x[ostatni_strzal - 10]._Strzal == false) && (ostatni_strzal - 10 >= 0))
			{
				r = ostatni_strzal - 10;
			}
			else if ((x[ostatni_strzal - 10]._Trafienie == true) && (x[ostatni_strzal - 20]._Strzal == false) && (ostatni_strzal - 20 >= 0))
			{
				r = ostatni_strzal - 20;
			}
			else if ((x[ostatni_strzal - 20]._Trafienie == true) && (x[ostatni_strzal - 30]._Strzal == false) && (ostatni_strzal - 30 >= 0))
			{
				r = ostatni_strzal - 30;
			}
			else if ((x[ostatni_strzal + 10]._Strzal == false) && (ostatni_strzal + 10 < 100))
			{
				r = ostatni_strzal + 10;
			}
			else if ((x[ostatni_strzal + 10]._Trafienie == true) && (x[ostatni_strzal + 20]._Strzal == false) && (ostatni_strzal + 20 < 100))
			{
				r = ostatni_strzal + 20;
			}
			else if ((x[ostatni_strzal - 20]._Trafienie == true) && (x[ostatni_strzal + 30]._Strzal == false) && (ostatni_strzal + 30 < 100))
			{
				r = ostatni_strzal + 30;
			}
		}

		if (x[r]._Strzal == false)
		{
			if (x[r]._Ukryty == true)
			{
				x[r]._Strzal = true;
				x[r]._Trafienie = true;
				losowanie = false;
				_ID = x[r]._ID_statku;
				c = x[r]._Wspolrzedna;
				l--;
				strzal_wykonany = false;

				kolor_tekstu(10);
				cout << endl << "[Sztuczna Inteligencja] Trafiono TWOJ statek!";
				kolor_tekstu(15);

				if (ostatni_strzal == 0)
				{
					ostatni_strzal = r;
				}

				Sleep(1000);
				return true;
			}
			else
			{
				x[r]._Strzal = true;
				x[r]._Pudlo = true;
				losowanie = false;
				strzal_wykonany = true;

				kolor_tekstu(11);
				cout << endl << "[Sztuczna Inteligencja] Pudlo!";
				Sleep(1000);
				kolor_tekstu(15);
			}
		}
		else
		{
			losowanie = true;
		}
	} while (losowanie == true);

	return false;
}

void f_przenies_kursor(CPlansza x[], int &_Kursor_do_poruszania, bool &end)
{
	int klawisz;

	klawisz = _getch();

	switch (klawisz)
	{
	case 72: //w gore
		if (_Kursor_do_poruszania - 10 >= 0)
		{
			x[_Kursor_do_poruszania]._Kursor_do_poruszania = false;
			_Kursor_do_poruszania -= 10;
			x[_Kursor_do_poruszania]._Kursor_do_poruszania = true;
			end = false;
			break;
		}

	case 80: //w dol
		if (_Kursor_do_poruszania + 10 < 100)
		{
			x[_Kursor_do_poruszania]._Kursor_do_poruszania = false;
			_Kursor_do_poruszania += 10;
			x[_Kursor_do_poruszania]._Kursor_do_poruszania = true;
			end = false;
			break;
		}

	case 75: //w lewo
		if (_Kursor_do_poruszania - 1 >= 0)
		{
			x[_Kursor_do_poruszania]._Kursor_do_poruszania = false;
			_Kursor_do_poruszania--;
			x[_Kursor_do_poruszania]._Kursor_do_poruszania = true;
			end = false;
			break;
		}

	case 27: 
	{
		wyczysc_ekran();
		cout << "WYSZEDLES Z GRY! \n";
		Sleep(700);
		exit(0);
	}

	case 77: //w prawo
		if (_Kursor_do_poruszania + 1 < 100)
		{
			x[_Kursor_do_poruszania]._Kursor_do_poruszania = false;
			_Kursor_do_poruszania++;
			x[_Kursor_do_poruszania]._Kursor_do_poruszania = true;
			end = false;
			break;
		}

	case 32: //strzal
		end = true;
		break;
	}
}

// METODY

void CPlansza::_m_Ustaw_kursor(int &aktualna_pozycja_kursora)
{
	_Kursor_do_poruszania = true;
	aktualna_pozycja_kursora = 0;
}

void CStatek::_m_Ustaw_statek(CPlansza x[])
{
	bool wolne_gora, wolne_prawa, losowanie;
	bool gora[8], prawa[8];
	int r, droga;
	srand(time(NULL));

	bool czy_moge_ustawic;

	do
	{
		r = rand() % 100;

		if ((r >= 0) && (r < 100))
		{
			if (r + (_Dlugosc*(-10)) > 0)
			{
				for (int i = 0; i >= -10 * _Dlugosc; i -= 10)
				{
					if (x[r + i]._Ukryty == false)
					{
						czy_moge_ustawic = true;
					}
					else
					{
						czy_moge_ustawic = false;
						break;
					}
				}
			}
			else
			{
				czy_moge_ustawic = false;
			}

			if (czy_moge_ustawic == true) // <- rozpoczęcie sprawdzania góry
			{
				if (r - (10 * _Dlugosc) - 10 >= 0) // <- sprawdzanie góry
				{
					if (x[r - (10 * _Dlugosc) - 10]._Ukryty == false)
					{
						gora[0] = true;
					}
					else
					{
						gora[0] = false;
					}
				}
				else
				{
					gora[0] = true;
				}

				if (r + 10 < 100) // <- sprawdzanie dołu
				{
					if (x[r + 10]._Ukryty == false)
					{
						gora[1] = true;
					}
					else
					{
						gora[1] = false;
					}
				}
				else
				{
					gora[1] = true;
				}

				if ((r % 10 != 0) && (r - (10 * _Dlugosc) - 1 >= 0)) // <- sprawdzanie lewej
				{
					for (int i = -1; i >= (-10 * _Dlugosc) - 1; i -= 10)
					{
						if (x[r + i]._Ukryty == false)
						{
							gora[2] = true;
						}
						else
						{
							gora[2] = false;
							break;
						}
					}
				}
				else
				{
					gora[2] = true;
				}

				if (((r + 1) % 10 != 0) && (r - (10 * _Dlugosc) + 1 > 0)) // <- sprawdzanie prawej
				{
					for (int i = 1; i >= (-10 * _Dlugosc + 1); i -= 10)
					{
						if (x[r + i]._Ukryty == false)
						{
							gora[3] = true;
						}
						else
						{
							gora[3] = false;
							break;
						}
					}
				}
				else
				{
					gora[3] = true;
				}

				if ((r - (10 * _Dlugosc) - 1 >= 0) && (r % 10 != 0)) // <- sprawdzanie lewego górnego rogu
				{
					if (x[r - (10 * _Dlugosc) - 11]._Ukryty == false)
					{
						gora[4] = true;
					}
					else
					{
						gora[4] = false;
					}
				}
				else
				{
					gora[4] = true;
				}

				if ((r - (10 * _Dlugosc) + 10 >= 0) && ((r + 1) % 10 != 0)) // <- sprawdzanie prawego górnego rogu
				{
					if (x[r - (10 * _Dlugosc) - 9]._Ukryty == false)
					{
						gora[5] = true;
					}
					else
					{
						gora[5] = false;
					}
				}
				else
				{
					gora[5] = true;
				}

				if ((r + 10 < 100) && (r % 10 != 0)) // <- sprawdzanie lewego dolnego rogu
				{
					if (x[r + 9]._Ukryty == false)
					{
						gora[6] = true;
					}
					else
					{
						gora[6] = false;
					}
				}
				else
				{
					gora[6] = true;
				}

				if ((r + 10 < 100) && ((r + 1) % 10 != 0)) // <- sprawdzanie prawego dolnego rogu
				{
					if (x[r + 11]._Ukryty == false)
					{
						gora[7] = true;
					}
					else
					{
						gora[7] = false;
					}
				}
				else
				{
					gora[7] = true;
				}
			} // <- koniec sprawdzania góry
			else
			{
				for (int i = 0; i < 8; i++)
				{
					gora[i] = false;
				}
			}

			if (r + _Dlugosc < 100)
			{
				for (int i = 0; i < _Dlugosc + 1; i++)
				{
					if (i == 0)
					{
						if (x[r + i]._Ukryty == false)
						{
							czy_moge_ustawic = true;
						}
						else
						{
							czy_moge_ustawic = false;
							break;
						}
					}
					else
					{
						if ((x[r + i]._Ukryty == false) && ((r + i) % 10 != 0))
						{
							czy_moge_ustawic = true;
						}
						else
						{
							czy_moge_ustawic = false;
							break;
						}
					}
				}
			}
			else
			{
				czy_moge_ustawic = false;
			}

			if (czy_moge_ustawic == true) // <- rozpoczęcie sprawdzania prawej
			{
				if (r - 10 >= 0) // <- sprawdzanie góry
				{
					for (int i = -10; i <= -10 + _Dlugosc; i++)
					{
						if (x[r + i]._Ukryty == false)
						{
							prawa[0] = true;
						}
						else
						{
							prawa[0] = false;
							break;
						}
					}
				}
				else
				{
					prawa[0] = true;
				}

				if (r + 10 < 100) // <- sprawdzanie dołu
				{
					for (int i = 10; i <= _Dlugosc + 10; i++)
					{
						if (x[r + i]._Ukryty == false)
						{
							prawa[1] = true;
						}
						else
						{
							prawa[1] = false;
							break;
						}
					}
				}
				else
				{
					prawa[1] = true;
				}

				if ((r % 10 != 0) && (r - 1 >= 0)) // <- sprawdzanie lewej
				{
					if (x[r - 1]._Ukryty == false)
					{
						prawa[2] = true;
					}
					else
					{
						prawa[2] = false;
					}
				}
				else
				{
					prawa[2] = true;
				}

				if (((r + _Dlugosc + 1) % 10 != 0) && (r + _Dlugosc + 1 < 100)) // <- sprawdzanie prawej
				{
					if (x[r + _Dlugosc + 1]._Ukryty == false)
					{
						prawa[3] = true;
					}
					else
					{
						prawa[3] = false;
					}
				}
				else
				{
					prawa[3] = true;
				}

				if ((r % 10 != 0) && (r - 10 >= 0)) // <- sprawdzanie lewego górnego rogu
				{
					if (x[r - 11]._Ukryty == false)
					{
						prawa[4] = true;
					}
					else
					{
						prawa[4] = false;
					}
				}
				else
				{
					prawa[4] = true;
				}

				if (((r + 1) % 10 != 0) && (r - 10 >= 0)) // <- sprawdzanie prawego górnego rogu
				{
					if (x[r - 9 + _Dlugosc]._Ukryty == false)
					{
						prawa[5] = true;
					}
					else
					{
						prawa[5] = false;
					}
				}
				else
				{
					prawa[5] = true;
				}

				if ((r % 10 != 0) && (r + 10 < 100)) // <- sprawdzanie lewego dolnego rogu
				{
					if (x[r + 9]._Ukryty == false)
					{
						prawa[6] = true;
					}
					else
					{
						prawa[6] = false;
					}
				}
				else
				{
					prawa[6] = true;
				}

				if (((r + 1) % 10 != 0) && (r + 10 < 100)) // <- sprawdzanie prawego dolnego rogu
				{
					if (x[r + 11 + _Dlugosc]._Ukryty == false)
					{
						prawa[7] = true;
					}
					else
					{
						prawa[7] = false;
					}
				}
				else
				{
					prawa[7] = true;
				}
			}
			else
			{
				for (int i = 0; i < 8; i++)
				{
					prawa[i] = false;
				}
			}
		}
		else
		{
			for (int i = 0; i < 8; i++)
			{
				prawa[i] = false;
				gora[i] = false;
			}
		}

		for (int i = 0; i < 8; i++)
		{
			if (gora[i] == true)
			{
				wolne_gora = true;
			}
			else
			{
				wolne_gora = false;
				break;
			}
		}

		for (int i = 0; i < 8; i++)
		{
			if (prawa[i] == true)
			{
				wolne_prawa = true;
			}
			else
			{
				wolne_prawa = false;
				break;
			}
		}

		if ((wolne_prawa == true) && (wolne_gora == true))
		{
			droga = rand() % 2 + 1;
			losowanie = false;
		}
		else if ((wolne_gora == true) && (wolne_prawa == false))
		{
			droga = 1;
			losowanie = false;
		}
		else if ((wolne_gora == false) && (wolne_prawa == true))
		{
			droga = 2;
			losowanie = false;
		}
		else
		{
			losowanie = true;
		}
	} while (losowanie == true);

	if (droga == 1)
	{
		int j = 1;
		for (int i = 0; i <= _Dlugosc * 10; i += 10)
		{
			x[r - i]._Ukryty = true;
			x[r - i]._ID_statku = _ID;
			x[r - i]._Wspolrzedna = j;
			x[r - i]._Wlasciciel_statku = _Wlasciciel;
			j++;
		}
	}
	else if (droga == 2)
	{
		int j = 1;
		for (int i = 0; i <= _Dlugosc; i++)
		{
			x[r + i]._Ukryty = true;
			x[r + i]._ID_statku = _ID;
			x[r + i]._Wspolrzedna = j;
			x[r + i]._Wlasciciel_statku = _Wlasciciel;
			j++;
		}
	}
}

void CLotniskowiec::_vm_Atak(int x, int &ostatni_strzal)
{
	switch (x)
	{
	case 1:
		this->_trafienie_1 = true;
		break;
	case 2:
		this->_trafienie_2 = true;
		break;
	case 3:
		this->_trafienie_3 = true;
		break;
	case 4:
		this->_trafienie_4 = true;
		break;
	}

	if ((_trafienie_1 == true) && (_trafienie_2 == true) && (_trafienie_3 == true) && (_trafienie_4 == true) && (_Czy_zatopiony == false))
	{
		if (_Wlasciciel == 2)
		{
			kolor_tekstu(12);
			cout << endl << "[GRACZ] Utracono czteromasztowiec!";
			kolor_tekstu(15);
			ostatni_strzal = 0;
			_Czy_zatopiony = true;
		}
		else
		{
			kolor_tekstu(11);
			cout << endl << "Sztuczna Inteligencja utraciła czteromasztowiec!";
			kolor_tekstu(15);
			_Czy_zatopiony = true;
		}
		Sleep(1000);
	}
}

void CKrazownik::_vm_Atak(int x, int &ostatni_strzal)
{
	switch (x)
	{
	case 1:
		this->_trafienie_1 = true;
		break;
	case 2:
		this->_trafienie_2 = true;
		break;
	case 3:
		this->_trafienie_3 = true;
		break;
	}

	if ((_trafienie_1 == true) && (_trafienie_2 == true) && (_trafienie_3 == true) && (_Czy_zatopiony == false))
	{
		if (_Wlasciciel == 2)
		{
			kolor_tekstu(12);
			cout << endl << "[GRACZ] Utracono trojmasztowiec!";
			kolor_tekstu(15);
			ostatni_strzal = 0;
			_Czy_zatopiony = true;
		}
		else
		{
			kolor_tekstu(11);
			cout << endl << "Sztuczna Inteligencja utraciła trojmasztowiec!";
			kolor_tekstu(15);
			_Czy_zatopiony = true;
		}

		Sleep(1000);
	}
}

void CNiszczyciel::_vm_Atak(int x, int &ostatni_strzal)
{
	switch (x)
	{
	case 1:
		this->_trafienie_1 = true;
		break;
	case 2:
		this->_trafienie_2 = true;
		break;
	}

	if ((_trafienie_1 == true) && (_trafienie_2 == true) && (_Czy_zatopiony == false))
	{
		if (_Wlasciciel == 2)
		{
			kolor_tekstu(12);
			cout << endl << "[GRACZ] Utracono dwumasztowiec!";
			kolor_tekstu(15);
			ostatni_strzal = 0;
			_Czy_zatopiony = true;
		}
		else
		{
			kolor_tekstu(11);
			cout << endl << "Sztuczna Inteligencja utraciła dwumasztowiec!";
			kolor_tekstu(15);
			_Czy_zatopiony = true;
		}
		Sleep(1000);
	}
}

// KONSTRUKTORY
CPlansza::CPlansza()
{
	_ID_statku = 0;
	_Wspolrzedna = 0;
	_Ukryty = 0;
	_Strzal = 0;
	_Trafienie = 0;
	_Pudlo = 0;
	_Kursor_do_poruszania = false;
}

CLotniskowiec::CLotniskowiec(int a, int b)
{
	_ID = a;
	_Wlasciciel = b;

	_Czy_zatopiony = false;

	_trafienie_1 = false;
	_trafienie_2 = false;
	_trafienie_3 = false;
	_trafienie_4 = false;

	_Dlugosc = 3;
}

CKrazownik::CKrazownik(int a, int b)
{
	_ID = a;
	_Wlasciciel = b;

	_Czy_zatopiony = false;

	_trafienie_1 = false;
	_trafienie_2 = false;
	_trafienie_3 = false;

	_Dlugosc = 2;
}

CNiszczyciel::CNiszczyciel(int a, int b)
{
	_ID = a;
	_Wlasciciel = b;

	_Czy_zatopiony = false;

	_trafienie_1 = false;
	_trafienie_2 = false;

	_Dlugosc = 1;
}