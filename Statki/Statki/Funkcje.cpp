#include "Funkcje.h"
#include "Klasa_statki.h"

// ---------- MENU ----------
void menu()
{
	zmiana_rozmiaru_okna(120, 30);
	string menu_tekst[] = { "----------------------- MENU: -----------------------",
							"                                                     ",
							"1. Rozpocznij gre",
							"2. Instrukcja",
							"3. Autorzy",
							"4. Wyjscie",
							"                                                     ",
							"-----------------------------------------------------",
							"Twoj wybor: " };

	COORD wsp;
	wsp.X = 0;
	wsp.Y = 10;

	wyczysc_ekran();
	powitanie();
	cout << endl << endl << endl << endl;
	for (;;)
	{
		for (unsigned short int i = 0; i < 9; i++)
		{
			SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), wsp);
			cout << menu_tekst[i] << endl;
			wsp.Y = wsp.Y + 1;
		}

		unsigned short int wybor;
		cin >> wybor;
		switch (wybor)
		{
		case 1:
			GRA(); break;
		case 2:
			instrukcja(); break;
		case 3:
			pokaz_autorow(); break;
		case 4:
			exit(0); break;
		default:
			cout << "\nBrak takiej opcji\n\n";
			tekst_powrot_do_menu(); _getch();
			wyczysc_ekran();; break;
		}
	}
}

void pokaz_autorow()
{
	wyczysc_ekran();
	cout << "Autorzy:\n";
	cout << " - Stachnik Paulina\n - Zahradnik Jakub\n\n";
	tekst_powrot_do_menu();
	_getch();
	menu();
}

void instrukcja()
{
	wyczysc_ekran();
	cout << "------------- INSTRUKCJA ------------- \n\n";
	kolor_tekstu(10);
	cout << "	1. Sterowanie:\n";
	kolor_tekstu(15);
	cout << "		- Sterowanie za pomoca strzalek\n";
	cout << "		- Strzal - SPACJA\n\n";
	cout << "		- Wyjscie z gry - ESC\n\n";
	kolor_tekstu(10);
	cout << "	2. Zasady:\n";
	kolor_tekstu(15);
	cout << "		a) Dostepne statki:\n";
	cout << "			- 1 lotniskowiec ( 4 pola \ 1 statek )\n";
	cout << "			- 2 krazowniki   ( 3 pola \ 1 statek )\n";
	cout << "			- 3 niszczyciele ( 2 pola \ 1 statek )\n\n";

	cout << "		b) Jak gra AI:\n";
	cout << "			- Pierwszy strzal AI jest zawsze losowy. W zaleznosci od jego wyniku algorytm podejmuje nastepujace kroki:\n";
	cout << "				* Jesli trafiony, to losuj miejsce nad, pod, po lewej albo po prawej i strzel. \n";
	cout << "				* Jesli ponownie trafiony, to kontynuuj strzelanie na pozcyji +1 lub -1 wzgledem poprzedniego tafienia \n";
	cout << "				* Jesli pudlo, to znowu losuj pole\n\n\n";

	tekst_powrot_do_menu();
	cout << endl;
	_getch();
	menu();
}

void powitanie()
{
	string powitanie[] = { "  ______   _________     _     _________  ___  ____   _____  ",
						   ".' ____ \\ |  _   _  |   / \\   |  _   _  ||_  ||_  _| |_   _| ",
						   "| (___ \\_||_/ | | \\_|  / _ \\  |_/ | | \\_|  | |_/ /     | |   ",
						   " _.____`.     | |     / ___ \\     | |      |  __'.     | |   ",
						   "| \\____) |   _| |_  _/ /   \\ \\_  _| |_    _| |  \\ \\_  _| |_  ",
						   " \\______.'  |_____||____| |____||_____|  |____||____||_____| " };

	string powitanie2[] = { "              |    |    |                 ",
						   "             )_)  )_)  )_)                ",
						   "            )___))___))___)\              ",
						   "           )____)____)_____)\\            ",
						   "         _____|____|____|____\\\__        ",
						   " ---------\                   /---------  ",
						   "  ^^^^^ ^^^^^^^^^^^^^^^^^^^^^             ",
						   "     ^^^^      ^^^^     ^^^    ^^         ",
						   "          ^^^^      ^^^                   " };
	kolor_tekstu(11);
	for (int i = 0; i < 6; i++)
	{
		cout << powitanie[i] << endl;
	}

	COORD wsp;
	wsp.X = 75;
	wsp.Y = 10;

	for (int i = 0; i < 9; i++)
	{
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), wsp);
		cout << powitanie2[i] << endl;
		wsp.Y = wsp.Y + 1;
	}

	kolor_tekstu(15);
}

// ---------- GRA ----------
void GRA()
{
	zmiana_rozmiaru_okna(50, 40);
	wyczysc_ekran();
	CPlansza *Pole1 = new CPlansza[100];
	CPlansza *Pole2 = new CPlansza[100];

	int aktualna_pozycja_kursora;
	Pole1[0]._m_Ustaw_kursor(aktualna_pozycja_kursora);

	CLotniskowiec  Lotniskowiec_gracz(1, 1);
	CKrazownik   Krazownik_1_gracz(2, 1);
	CKrazownik   Krazownik_2_gracz(3, 1);
	CNiszczyciel Niszczyciel_1_gracz(4, 1);
	CNiszczyciel Niszczyciel_2_gracz(5, 1);
	CNiszczyciel Niszczyciel_3_gracz(6, 1);
	CLotniskowiec  Lotniskowiec_AI(7, 2);
	CKrazownik   Krazownik_1_AI(8, 2);
	CKrazownik   Krazownik_2_AI(9, 2);
	CNiszczyciel Niszczyciel_1_AI(10, 2);
	CNiszczyciel Niszczyciel_2_AI(11, 2);
	CNiszczyciel Niszczyciel_3_AI(12, 2);

	Lotniskowiec_gracz._m_Ustaw_statek(Pole1);
	Krazownik_1_gracz._m_Ustaw_statek(Pole1);
	Krazownik_2_gracz._m_Ustaw_statek(Pole1);
	Niszczyciel_1_gracz._m_Ustaw_statek(Pole1);
	Niszczyciel_2_gracz._m_Ustaw_statek(Pole1);
	Niszczyciel_3_gracz._m_Ustaw_statek(Pole1);

	Lotniskowiec_AI._m_Ustaw_statek(Pole2);
	Krazownik_1_AI._m_Ustaw_statek(Pole2);
	Krazownik_2_AI._m_Ustaw_statek(Pole2);
	Niszczyciel_1_AI._m_Ustaw_statek(Pole2);
	Niszczyciel_2_AI._m_Ustaw_statek(Pole2);
	Niszczyciel_3_AI._m_Ustaw_statek(Pole2);

	int _ID, _Wspolrzedna;
	int pozostalo_graczowi = 16;
	int pozostalo_AI = 16;
	int proby_strzalow = 1;
	int ostatni_strzal = 0;
	bool strzal_wykonany, koniec_petli, _Trafienie;
	bool koniec_gry = false;
	CStatek *s;

	do
	{
		strzal_wykonany = false;

		while (strzal_wykonany == false)
		{
			koniec_petli = false;

			while (koniec_petli == false)
			{
				wyczysc_ekran();
				legenda_pol();
				kolor_tekstu(12);
				cout << " ------------- POLE WROGA --------------\n" << endl;
				kolor_tekstu(15);
				f_ryruj_plansze(Pole1);
				cout << endl;
				kolor_tekstu(9);
				cout << endl << " ------------- TWOJE POLE --------------\n";
				cout << endl;
				kolor_tekstu(15);
				f_ryruj_plansze(Pole2);
				informacje_o_polach(pozostalo_AI, pozostalo_graczowi, proby_strzalow);
				f_przenies_kursor(Pole1, aktualna_pozycja_kursora, koniec_petli);
			}

			_Trafienie = f_strzal_gracza(Pole1, _ID, _Wspolrzedna, pozostalo_graczowi, proby_strzalow, aktualna_pozycja_kursora, strzal_wykonany);

			if (_Trafienie == true)
			{
				switch (_ID)
				{
				case 1:
					s = &Lotniskowiec_gracz;
					break;
				case 2:
					s = &Krazownik_1_gracz;
					break;
				case 3:
					s = &Krazownik_2_gracz;
					break;
				case 4:
					s = &Niszczyciel_1_gracz;
					break;
				case 5:
					s = &Niszczyciel_2_gracz;
					break;
				case 6:
					s = &Niszczyciel_3_gracz;
					break;
				}

				s->_vm_Atak(_Wspolrzedna, ostatni_strzal);
			}
		}

		if (pozostalo_graczowi == 0) koniec_gry = true;

		if (koniec_gry == false)
		{
			strzal_wykonany = false;

			while (strzal_wykonany == false)
			{
				wyczysc_ekran();
				legenda_pol();
				kolor_tekstu(12);
				cout << " ------------- POLE WROGA --------------\n" << endl;
				kolor_tekstu(15);
				f_ryruj_plansze(Pole1);
				cout << endl;
				kolor_tekstu(9);
				cout << endl << " ------------- TWOJE POLE --------------\n";
				cout << endl;
				kolor_tekstu(15);
				f_ryruj_plansze(Pole2);
				informacje_o_polach(pozostalo_AI, pozostalo_graczowi, proby_strzalow);

				_Trafienie = f_strzal_AI(Pole2, _ID, _Wspolrzedna, pozostalo_AI, ostatni_strzal, strzal_wykonany);

				if (_Trafienie == true)
				{
					switch (_ID)
					{
					case 7:
						s = &Lotniskowiec_AI;
						break;
					case 8:
						s = &Krazownik_1_AI;
						break;
					case 9:
						s = &Krazownik_2_AI;
						break;
					case 10:
						s = &Niszczyciel_1_AI;
						break;
					case 11:
						s = &Niszczyciel_2_AI;
						break;
					case 12:
						s = &Niszczyciel_3_AI;
						break;
					}

					s->_vm_Atak(_Wspolrzedna, ostatni_strzal);
				}
			}

			if (pozostalo_AI == 0) koniec_gry = true;
		}
	} while (koniec_gry == false);

	if (pozostalo_graczowi == 0) wygrana(proby_strzalow);
	else przegrana(proby_strzalow);
}

void legenda_pol()
{
	cout << " ------------ LEGENDA: ------------\n\n";
	kolor_tekstu(10); cout << " Trafiony     ";
	kolor_tekstu(12); cout << "Pudlo     ";
	kolor_tekstu(9); cout << "Twoj statek " << endl << endl << endl;
	kolor_tekstu(15); cout << endl;
}

void informacje_o_polach(int l1, int l2, int t)
{
	kolor_tekstu(15); cout << endl << "Pola zajete przez Twoje statki: "; kolor_tekstu(10); cout << l1;
	kolor_tekstu(15); cout << endl << "Pola zajete przez statki AI:  "; kolor_tekstu(10); cout << l2;
	cout << endl;
	kolor_tekstu(15);
}

void wygrana(int t)
{
	wyczysc_ekran(); kolor_tekstu(10);
	cout << endl << "Gratulacje! Wygrales po  " << t << " strzalach!" << endl;
	stop();
	menu();
}

void przegrana(int t)
{
	wyczysc_ekran(); kolor_tekstu(12);
	cout << endl << "Przegrales po " << t << " strzalach przeciwnika!" << endl;
	stop();
	menu();
}

// ---------- Funkcje pomocnicze ----------
void zmiana_rozmiaru_okna(short szerokosc, short wysokosc)
{
	HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE);
	_SMALL_RECT Rect;
	Rect.Top = 0;
	Rect.Left = 0;
	Rect.Bottom = wysokosc;
	Rect.Right = szerokosc;

	SetConsoleWindowInfo(Handle, TRUE, &Rect);
}

void tekst_powrot_do_menu()
{
	cout << "Aby wrocic do menu, wcisnij dowolny klawisz";
}

void kolor_tekstu(int c)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), c);
}

void wyczysc_ekran()
{
	system("CLS");
}

void stop()
{
	_getch();
}