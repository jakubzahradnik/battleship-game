#include <iostream>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include <string>

using namespace std;

//funkcje wirtualne, dziedziczenie :public CStatek

class CPlansza
{
	int _ID_statku;
	int _Wspolrzedna;
	int _Wlasciciel_statku;
	bool _Ukryty;
	bool _Strzal;
	bool _Trafienie;
	bool _Pudlo;
	bool _Kursor_do_poruszania;

public:
	CPlansza();

	void _m_Ustaw_kursor(int &aktualna_pozycja_kursora);

	friend void f_ryruj_plansze(CPlansza x[]);
	friend void f_przenies_kursor(CPlansza x[], int &_Kursor_do_poruszania, bool &end);
	friend bool f_strzal_gracza(CPlansza *x, int &_ID, int &c, int &l, int &t, int y, bool &strzal_wykonany);
	friend bool f_strzal_AI(CPlansza *x, int &_ID, int &c, int &l, int &ostatni_strzal, bool &strzal_wykonany);

	friend class CStatek;
	friend class CLotniskowiec;
	friend class CKrazownik;
	friend class CNiszczyciel;
};

class CStatek
{
protected:

	int _ID;
	int _Wlasciciel;
	int _Dlugosc;
	bool _Czy_zatopiony;

public:
	void _m_Ustaw_statek(CPlansza x[]);
	virtual void _vm_Atak(int x, int &ostatni_strzal) = 0; // funkcja wirtualna
};

class CLotniskowiec :public CStatek
{
	bool _trafienie_1;
	bool _trafienie_2;
	bool _trafienie_3;
	bool _trafienie_4;

public:

	CLotniskowiec(int a, int b);

	virtual void _vm_Atak(int x, int &ostatni_strzal);
};

class CKrazownik :public CStatek
{
	bool _trafienie_1;
	bool _trafienie_2;
	bool _trafienie_3;

public:

	CKrazownik(int a, int b);

	virtual void _vm_Atak(int x, int &ostatni_strzal);
};

class CNiszczyciel :public CStatek
{
	bool _trafienie_1;
	bool _trafienie_2;

public:

	CNiszczyciel(int a, int b);

	virtual void _vm_Atak(int x, int &ostatni_strzal);
};

void f_ryruj_plansze(CPlansza x[]);
bool f_strzal_gracza(CPlansza *x, int &_ID, int &c, int &l, int &t, int y, bool &strzal_wykonany);
bool f_strzal_AI(CPlansza *x, int &_ID, int &c, int &l, int &ostatni_strzal, bool &strzal_wykonany);
void f_przenies_kursor(CPlansza x[], int &_Kursor_do_poruszania, bool &end);
